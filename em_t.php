<h3>empty example 1 </h3>

<?php
$var = 0;
if(empty($var)){
    echo '$var is either 0,empty, or not set at all';
}
if(isset($var)){
    echo '$var is set even though it is empty';
}


?>

<h3>empty example 2 </h3>
<?php
$expected_array_got_string='somestring';
var_dump(empty($expected_array_got_string['some_key']));
var_dump(empty($expected_array_got_string[0]));
var_dump(empty($expected_array_got_string['0']));
var_dump(empty($expected_array_got_string[0.5]));
var_dump(empty($expected_array_got_string['0.5']));
var_dump(empty($expected_array_got_string['0 Mostel']));

?>