<h3>ISSET Example 1</h3>
<?php
//This will evaluation to True so the text will printing
if(isset($var)){
    echo"This var is set so i will print";
}
$a="test";
$b="anather test";
var_dump(isset($a));   //true
var_dump(isset($a,$b));  //true
unset($a);
var_dump(isset($a));  //false
var_dump(isset($a,$b));  //false
$FOO=null;
var_dump(isset($FOO));//false
?>


<h3>isset example 2</h3>
<?php
$a=array('test'=>1,'hello'=>null,'pie'=>array('a'=>'apple'));
var_dump(isset($a['test']));  //true
var_dump(isset($a['Foo']));   //false
var_dump(isset($a['hello'])); //true
//the key hello equal null so is consider unset
//if you want to check for null key value then try
var_dump(array_key_exists('hello',$a)); //true
// checking deeper array values
var_dump(isset($a['pie']['a']));
var_dump(isset($a['pie']['b']));
var_dump(isset($a['cake']['a']['b']));
?>